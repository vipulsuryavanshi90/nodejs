var express = require('express');
var jwt = require('jsonwebtoken');
var config = require('./../config');
var Cryptr = require('cryptr');
const cryptr = new Cryptr('ExeRandomTotalySecretKey');
var router = express.Router();

//User Registration
router.post('/add', function(req,res){
	req.assert('username', 'Username is required').notEmpty();
    req.assert('password', 'Password is required').notEmpty();

    var errors = req.validationErrors();

    if(!errors){

    	var user = {
	        "username":req.body.username,
	        "password":req.body.password//cryptr.encrypt(req.body.password)
    	}
    	config.connection.query('select * from user where username = ?', [req.body.username], function(err,results){
    		if(err){	
    			res.json({status:false, message:'error:'+ err});
    		}
    		else if(results.length > 0){

    			res.json({status:true, data:results, message:'username already exists'});
    		}
    		else{

    			config.connection.query('insert into user SET ?',user, function(err,results){

		    		if(err){
		    				res.json({status:false,message:'error:'+ err});
		    			}
		    		else{
		    				res.json({status:true, data:results, message:'user registered sucessfully'});
		    		}
		    	});

    		}
    	});
    }else{
    	var error_msg = ''
        errors.forEach(function(error) {
            error_msg += error.msg + ','
        })   
        res.json({status:false, message:error_msg});
    }
});




router.post('/login', function(req, res) {
	req.assert('username', 'username is required').notEmpty();
	req.assert('password', 'password is required').notEmpty();

	var errors = req.validationErrors();
	if(!errors){
		var password = req.body.password;
		var username  =req.body.username;
		var passwordHash = password;//cryptr.decrypt(req.body.password);
	    config.connection.query('select * from user where username = ? AND password = ?', [username,passwordHash], function(err,results){
	    	if(err){
	    		res.json({status:false, message:'error:'+ err});
	    	}
	    	else if(results.length > 0){
	    		
				var token = jwt.sign({ id: results[0].id }, config.secretKey.secret, {
	  				expiresIn: 86400 // expires in 24 hours
				});

				res.json({status:true, token:token, message:'sucessfully signIn'});
	    	}else{

	    		res.json({status:false, data:results, message:'signIn Failed Please Provide Correct Cridentuals'});

	    		}
	    });

	}else{
		var error_msg = ''
        errors.forEach(function(error) {
            error_msg += error.msg + ','
        })   
        res.json({status:false, message:error_msg});
	}

});

module.exports = router;