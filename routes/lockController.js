var express = require('express');
var config = require('./../config');
var router = express.Router();
const uuid = require('uuid/v1');
var VerifyToken = require('./../middlewares');

router.post('/add', VerifyToken,function(req,res){
	req.assert('name', 'Lock Name is required').notEmpty();
	var errors = req.validationErrors();
	
	if(!errors){
		var unique_id = uuid();
		var lock = {
			"macId":unique_id,
	        "name":req.body.name,
	        "user": req.user_id
	        
    	}
		 config.connection.query('select * from locks where name = ?', [req.body.name], function(err,results){
			if(err){	
    			res.json({status:false, message:'error:'+ err});
    		}
    		else if(results.length > 0){

    			res.json({status:false, data:results, message:'Lock name already exists'});
    		}else{
    			config.connection.query('insert into locks SET ?',lock, function(err,results){

		    		if(err){
		    				res.json({status:false,message:'error:'+ err});
		    			}
		    		else{
		    				res.json({status:true, data:results, message:'Lock Created sucessfully'});
		    		}
		    	});
    		}
		});
		
	}
	else{
        var error_msg = ''
        errors.forEach(function(error) {
            error_msg += error.msg + ','
        })   
        res.json({status:false, message:error_msg});
    }
});

router.put('/update',VerifyToken,function(req,res){

    req.assert('name', 'Lock Name is required').notEmpty();
    

    var lock = {
            "name": req.body.name,
        }

    var errors = req.validationErrors();
    if(!errors){

    config.connection.query('select * from locks where id = ?', [req.body.lock_id], function(err,results){
            if(err){    
                res.json({status:false, message:'error:'+ err});
            }
            else if(results.length > 0){

                config.connection.query('update locks SET ? where id = ?',[lock,req.body.lock_id], function(err,results){

                    if(err){
                            res.json({status:false,message:'error:'+ err});
                        }
                    else{
                            res.json({status:true, data:results, message:'Lock Updated sucessfully'});
                    }
                });

            }else{
            	res.json({status:false, message:'Lock Not Found'});
            }
           
        });

    }else{
        var error_msg = ''
        errors.forEach(function(error) {
            error_msg += error.msg + ','
        })   
        res.json({status:false, message:error_msg});
    }

});

router.delete('/delete',VerifyToken,function(req,res){

 
    config.connection.query('select * from locks where id = ?', [req.body.lock_id], function(err,results){
            if(err){    
                res.json({status:false, message:'error:'+ err});
            }
            else if(results.length > 0){

                config.connection.query('delete from locks where id = ?',[req.body.lock_id], function(err,results){

                    if(err){
                            res.json({status:false,message:'error:'+ err});
                        }
                    else{
                            res.json({status:true, data:results, message:'Lock Deleted sucessfully'});
                    }
                });

            }else{
            	res.json({status:false, message:'Lock Not Found'});
            }
           
        });

});
router.get('/get',VerifyToken,function(req,res){

	config.connection.query('select * from locks where user = ?', [req.user_id], function(err,results){
			if(err){    
                res.json({status:false, message:'error:'+ err});
            }
            res.json({status:true,data:results,message:'User locks'});
	});
});

module.exports = router;
