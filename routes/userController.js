var express = require('express');
var config = require('./../config');
var router = express.Router();

var VerifyToken = require('./../middlewares');


//user Update
router.put('/update',VerifyToken,function(req,res){

    req.assert('name', 'name is required').notEmpty();
    req.assert('dob', 'dob is required').notEmpty();
    
    var user = {
            "name": req.body.name,
            "dob": req.body.dob
        }

    var errors = req.validationErrors();
    if(!errors){

    config.connection.query('select * from user where id = ?', [req.user_id], function(err,results){
            if(err){    
                res.json({status:false, message:'error:'+ err});
            }
            else if(results.length > 0){

                config.connection.query('update user SET ? where id = ?',[user,req.user_id], function(err,results){

                    if(err){
                            res.json({status:false,message:'error:'+ err});
                        }
                    else{
                            res.json({status:true, data:results, message:'User Updated sucessfully'});
                    }
                });

            }
           
        });

    }else{
        var error_msg = ''
        errors.forEach(function(error) {
            error_msg += error.msg + ','
        })   
        res.json({status:false, message:error_msg});
    }

});


router.put('/delete',VerifyToken,function(req,res){

    config.connection.query('select * from user where id = ?', [req.user_id], function(err,results){
            if(err){    
                res.json({status:false, message:'error:'+ err});
            }
            else if(results.length > 0){

                config.connection.query('delete from user where id = ?',[req.user_id], function(err,results){

                    if(err){
                            res.json({status:false,message:'error:'+ err});
                        }
                    else{
                            res.json({status:true, data:results, message:'User Deleted sucessfully'});
                    }
                });

            }
           
        });

    
});

module.exports = router;

