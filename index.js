
var express = require('express');
var users = require('./routes/userController');
var locks = require('./routes/lockController');
var auth = require('./routes/authController');

var bodyparser = require('body-parser');
var expressValidator = require('express-validator');
 
var app = express();

app.use(bodyparser.urlencoded({ extended: true}));
app.use(bodyparser.json({ type: 'application/json' }));
app.use(expressValidator());

app.use('/user',users);
app.use('/lock',locks);
app.use('/auth',auth);


app.listen(3000,()=>console.log('express sever running...'));
